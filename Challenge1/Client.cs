﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge1
{
    public class Client
    {
        private long telephoneNumber;
        private string serialOfDocument;
        private string firstName;
        private string middleName;
        private string lastName;
        private DateTime dateTime;
        private string changeLog;
        private string whoChange;

        public string FirstName { get { return firstName; } set { firstName = value; } }
        public string MiddleName { get { return middleName; } set { middleName = value; } }
        public string LastName { get { return lastName; } set { lastName = value; } }
        public DateTime DateTime { get { return dateTime; } set { dateTime = value; } }
        public string ChangeLog { get { return changeLog; } set { changeLog = value; } }
        public string WhoChange { get { return whoChange; } set { whoChange = value; } }
        public string SerialOfDocument { get { return serialOfDocument; } set { serialOfDocument = value; } }
        public long TelephoneNumber
        {
            get
            {
                return telephoneNumber;
            }
            set
            {
                if (value.ToString().Length >= 10) telephoneNumber = value;
            }
        }

        public Client(string firstName, string middleName, string lastName, long telephoneNumber, string serialOfDocument)
        {
            FirstName = firstName;
            MiddleName = middleName;
            LastName = lastName;
            TelephoneNumber = telephoneNumber;
            SerialOfDocument = serialOfDocument;
            dateTime = DateTime.Now;
            changeLog = "Новый клиент";
            whoChange = "Банк";
        }

    }
}
