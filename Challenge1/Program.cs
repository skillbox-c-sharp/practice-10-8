﻿using System.Reflection.Metadata;

namespace Challenge1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<Client> clientsList = SetClientsList();

            Consultant consultant = new Consultant("Консультант");
            Console.WriteLine(consultant.PrintClient(clientsList[0]));
            consultant.ChangeTelephoneNumber(clientsList[0], "1234567890");
            Console.WriteLine(consultant.PrintClient(clientsList[0]));


            Manager manager = new Manager("Менеджер");
            Console.WriteLine(manager.PrintClient(clientsList[2]));
            manager.ChangeTelephoneNumber(clientsList[2], "1234567890");
            manager.ChangeLastName(clientsList[2], "Паршин");
            Console.WriteLine(manager.PrintClient(clientsList[2]));

            manager.AddClient(new Client("Иван", "Александрович", "Семенов", 123344234234, "1234 567890"), clientsList);
            Console.WriteLine(manager.PrintClient(clientsList.Last()));
        }

        static List<Client> SetClientsList()
        {
            List<Client> clients = new List<Client>();

            for (int i = 0; i < 10; i++)
            {
                string firstName = $"Имя {i + 1}";
                string middleName = $"Отчество {i + 1}";
                string lastName = $"Фамилия {i + 1}";
                long phone = 01234567890;
                string passport = "1234 567890";

                clients.Add(new Client(firstName, middleName, lastName, phone, passport));
            }

            return clients;
        }
    }
}