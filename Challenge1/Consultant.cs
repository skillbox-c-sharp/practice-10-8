﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Challenge1
{
    public class Consultant : IEmployee
    {
        public string Name { get; set; }

        public Consultant(string name)
        {
            Name = name;
        }

        public virtual string PrintClient(Client client)
        {
            return client.FirstName + " " + client.MiddleName + " " + client.LastName +
                " " + "******************" + " " + client.TelephoneNumber + " " + client.DateTime + " " +
                client.ChangeLog + " " + client.WhoChange + "\n";
        }

        public void ChangeTelephoneNumber(Client client, string value)
        {
            client.TelephoneNumber = Convert.ToInt64(value);
            client.DateTime = DateTime.Now;
            client.ChangeLog += "Изменен номер телефона";
            client.WhoChange = Name;
        }
    }
}
