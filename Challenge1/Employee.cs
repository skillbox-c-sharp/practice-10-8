﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge1
{
    interface IEmployee
    {
        string Name { get; set; }
        void ChangeTelephoneNumber(Client client, string value);
        string PrintClient(Client client);
    }
}
