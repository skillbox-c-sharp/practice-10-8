﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Challenge1
{
    public class Manager : Consultant
    {
        public Manager(string name) : base(name) { }

        public void AddClient(Client client, List<Client> clientsList)
        {
            clientsList.Add(client);
        }

        public void ChangeFirstName(Client client, string firstName)
        {
            client.FirstName = firstName;
            client.DateTime = DateTime.Now;
            client.ChangeLog = "Изменено имя";
            client.WhoChange = Name;
        }

        public void ChangeMiddleName(Client client, string middleName)
        {
            client.MiddleName = middleName;
            client.DateTime = DateTime.Now;
            client.ChangeLog = "Изменено отчество";
            client.WhoChange = Name;
        }

        public void ChangeLastName(Client client, string lastName)
        {
            client.LastName = lastName;
            client.DateTime = DateTime.Now;
            client.ChangeLog = "Изменена фамилия";
            client.WhoChange = Name;
        }

        public void ChangeSerialOfDocument(Client client, string serialOfDocument)
        {
            client.SerialOfDocument = serialOfDocument;
            client.DateTime = DateTime.Now;
            client.ChangeLog = "Изменены паспортные данные";
            client.WhoChange = Name;
        }

        public override string PrintClient(Client client)
        {
            return client.FirstName + " " + client.MiddleName + " " + client.LastName +
                " " + client.SerialOfDocument + " " + client.TelephoneNumber.ToString() + " " + client.DateTime + " " +
                client.ChangeLog + " " + client.WhoChange + "\n";
        }
    }
}
